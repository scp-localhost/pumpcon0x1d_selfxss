# pumpcon0X1d_selfXSS

<p align="center">
  <a href="">
    <img src="images/logo.png" alt="Logo" width="600" height="300">
  </a>
  <h3 align="center">'self' XSS</h3>
  <p align="center">
  </p>
</p>
<!-- ToC -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About 'self' XSS Abuse...</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#scripts">Scripts</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

[![pumpcon0x1d_selfxss Screen Shot][product-screenshot]](images/screenshot.png)

...This started as a way to quick check "Hollywood CVE's", headers and library versions that may have been "hidden".

The behaviour of requests made by XMLHttp makes them more like browser extensions or scripts called asynchronously from inside the application.
Simple JavaScript snippets can emulate Burpsuite Intruder.
Asynchronous calls can load portions of the application warded by REST and DOM based validation but available to 'self'.
'headless CMS' is partucularly vulnerable; often lacking in server side compensatory controls (the headless part) and often even best practice defaults.
Content Security Policy may (probably) allow asynch calls to all subdomains. All applications under one domain...anything else in the CSP.

### Built With
* [JQuery](https://jquery.com)?? I guess...if you want.
* [malice?](https://youtu.be/dQw4w9WgXcQ)

<!-- GETTING STARTED -->
## Getting Started

~many sources for snippets
browser extensions

### Caveats

* :owl: Stan Lee. Audience considered; Samples are intrusive and potentially damaging depending on target selection.

* :owl: Impact to related systems not usually in line (or in scope) for testing? 

### Prerequisites

Chrome and some JS assumed?

### Installation

0. None. You can already do this in Chome (and others)

If you want...
1. Clone the repo
   ```sh
   
   git clone https://gitlab.com/scp-localhost/pumpcon0x1d_selfxss.git

   cd pumpcon0x1d_selfxss
   
   ```

<!-- scripts -->
## Scripts

 * :owl: allInOne.js:- all the js
 * :owl: samuelL.js:- sleep example
 * :owl: selfXSS_dirEnumjQ3_6.js:- jQuery based directory enumeration
 * :owl: selfXSS_dirEnum.js:- js based directory enumeration
 * :owl: selfXSS_get_a_href.js:- jQuery:- get/add anchors
 * :owl: selfXSS_injectForm.js:- inject form (browse for payload)
 * :owl: selfXSS_injectObjs.js
 * :owl: selfXSS_POST_upload.js:- XMLHttp POST
 * :owl: selfXSS_scrape.js:- Various environmental tests.
 * :owl: selfXSS_User_EnumjQ3_6.js:- Username & Password brute force

<!-- LICENSE -->
## License
Distributed under the MIT License. See `LICENSE` for more information.
Free for any use if you don't read it.
<!-- CONTACT -->
## Contact
[![Пасечник][product-Пасечник]](images/Пасечник.jpg)

scp - [@scp_localhost](https://twitter.com/scp_localhost)

Raoul.Duke@gotham.pw

*  :japanese_ogre:

In golf shirt/busines _casualty_, PG13, **business Sock's** at least please...[![LinkedIn][linkedin-shield]][linkedin-url]

Project Link: [https://gitlab.com/scp-localhost/pumpcon0x1d_selfxss.git](https://gitlab.com/scp-localhost/pumpcon0x1d_selfxss.git)

## About ~our Wednesday

[![Our Wednesday Screen Shot][product-wednesday]](images/wednesday.png)

...you've been warned


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Philly](https://www.nhl.com/flyers/fans/gritty)
* [John Hammond, for inspiration and the pptx payload you've already recieved.](https://github.com/JohnHammond)
* [Dr.Thompson](https://www.goodreads.com/quotes/1074-strange-memories-on-this-nervous-night-in-las-vegas-five)
* [George Gordon](https://en.wikisource.org/wiki/The_Works_of_Lord_Byron_(ed._Coleridge,_Prothero)/Poetry/Volume_3/Hebrew_Melodies/The_Destruction_of_Sennacherib)
* [my Daughter "Wednesday" for ink and gleeful malice]()
* [:owl:](https://www.temple.edu/)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://img.shields.io/static/v1?label=<LABEL>&message=<MESSAGE>&color=<COLOR> -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/steve-pote/
[product-screenshot]: images/screenshot.png
[product-Пасечник]: images/Пасечник.jpg
[product-wednesday]: images/wednesday.png
