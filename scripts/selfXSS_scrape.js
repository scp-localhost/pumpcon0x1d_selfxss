//selfXSS_scrape.js
function addListeners(xhr) {xhr.addEventListener('error', console.log(xhr.statusText));}
(function(){b="No Perms b0rk F5:- ";d=document;C=console;C.clear();u=window.location;
C.group("scrape");g="libs";C.group(g);
            j=$.fn===undefined ? "jQ?" :$.fn.jquery;
            a=''//(angular===undefined)? "Ang?" :angular.version.full;
            C.table("jQuery:"+j+" Angular:"+a);
C.groupEnd(g);g="cookieMonster";C.group(g);
if (d.cookie) {
      const cookies=d.cookie
        .split(/; ?/)
        .map(s => {
          const [ , key, value ] = s.match(/^(.*?)=(.*)$/);
          return {
            key,
            value: decodeURIComponent(value)
          };
        });
      C.table(cookies);
    }else{C.warn('no cookie!');}
C.groupEnd(g);g="elm";C.group(g);
  [].forEach.call(d.querySelectorAll("img"),function(i){
    c=d.createElement("canvas");
    ctx=c.getContext("2d");
    c.width=i.width;c.height=i.height;
    try {ctx.drawImage(i,0,0);C.log(i,c.toDataURL());}
    catch(e){C.log(i, b+e, i.src);}
  });
  [].forEach.call(d.querySelectorAll("canvas"), function(c){
    try {C.log(c, c.toDataURL());}
    catch(e){C.log(c, b+e);}
  });
  [].forEach.call(d.querySelectorAll("script"), function(c){
    try {C.log(c);}
    catch(e){C.log(c,b+e);}
  });
  [].forEach.call(d.querySelectorAll("form"), function(f){
    tab=[ ];
    C.group("HTMLForm \"" + f.name + "\": " + f.action);
    C.log("Elm:", f, "\nName:    "+f.name+"\nMethod:  "+f.method.toUpperCase()+"\nAction:  "+f.action || "null");
    ["input", "textarea", "select"].forEach(function (control) {
      [].forEach.call(f.querySelectorAll(control), function (node) {
        tab.push({
          "Elm": node,"Type": node.type,"Name":node.name,"Value":node.value,
          "Pretty Value": (isNaN(node.value)||node.value===""?node.value:parseFloat(node.value))
        });
      });
    });
    C.table(tab);
    C.groupEnd("HTMLForm \"" + f.name + "\": " + f.action);
  });
  C.groupEnd("elm");
  ["HEAD","GET","POST","PUT","DELETE","PRI","OPTIONS"].forEach(function(req){
    xhr=new XMLHttpRequest();
    addListeners(xhr);
    xhr.open(req,u,true);
    xhr.onload=function(){
      h=xhr.getAllResponseHeaders();
      tab=h.split("\n").map(function(i){
        return{"Key":i.split(": ")[0],"Value":i.split(": ")[1]}
    }).filter(function(i){return i.Value!== undefined;});
    C.group("Request "+req);C.table(tab);C.groupEnd("Request "+req);
  };
  xhr.send(null);
  });            
  C.groupEnd("scape");
  })();
